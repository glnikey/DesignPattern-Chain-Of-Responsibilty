import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class FileWorker {

	public File readFile(String path)
	{

		File f = new File(path);
		if(f.exists() && !f.isDirectory()) {

		    return f;

		}
		return null;
	}

	 public String getFileExtension(File file)
	 {
		 String fileName = file.getName();
	     if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
	     {
	    	 return fileName.substring(fileName.lastIndexOf(".")+1);
	     }
	     else
	     {
	    	 return "";
	     }
	 }

	 public boolean copyfile(File f, String path, String name)
	 {
		 try {

			Files.copy(f.toPath(),(new File(path + name+"."+getFileExtension(f))).toPath(), StandardCopyOption.REPLACE_EXISTING);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	 }

}
